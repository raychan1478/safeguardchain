﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Diagnostics;
using System.Threading;
using System.Net.Http;

using OpenCvSharp;
using OpenCvSharp.Extensions;
using System.Drawing;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Runtime.InteropServices;
using System.ComponentModel;

namespace SafeGuardChain
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : System.Windows.Window
    {
        private System.Windows.Forms.NotifyIcon MyNotifyIcon;

        // Session Variables
        private string loginToken;
        private string loginSessionID;
        private string globalApiEndpoint;
        
        // Face Capture
        private VideoCapture capture;
        private bool captureInProgress = true;
        
        private bool hearbeatFlag = true;
        private string copyPasteText = "Copy&Paste Disabled";

        // Background Worker
        private static BackgroundWorker heartbeatWorker = new BackgroundWorker();
        private static BackgroundWorker facialWorker = new BackgroundWorker();

        enum ExitFlags
        {
            Logoff = 0,
            Shutdown = 1,
            Reboot = 2,
            Force = 4,
            PowerOff = 8,
            ForceIfHung = 16
        }

        enum Reason : uint
        {
            ApplicationIssue = 0x00040000,
            HardwareIssue = 0x00010000,
            SoftwareIssue = 0x00030000,
            PlannedShutdown = 0x80000000
        }

        [DllImport("user32.dll", SetLastError = true)]
        static extern int ExitWindowsEx(uint uFlags, uint dwReason);

        [DllImport("user32.dll")]
        public static extern bool LockWorkStation();

        public MainWindow(string token, string sessionID, string userID, string apiEndpoint)
        {
            Trace.Listeners.Add(new TextWriterTraceListener(@"C:\safeguardchain.log"));
            Trace.AutoFlush = true;

            loginToken = token;
            Trace.WriteLine(loginToken);

            loginSessionID = sessionID;
            Trace.WriteLine(loginSessionID);
            Trace.WriteLine(userID);

            globalApiEndpoint = apiEndpoint;
            Trace.WriteLine(globalApiEndpoint);

            InitializeComponent();
            MyNotifyIcon = new System.Windows.Forms.NotifyIcon();
            MyNotifyIcon.Icon = new System.Drawing.Icon(
                            @"..\..\resources\safeguardchainlogo.ico");
            //this.WindowState = WindowState.Minimized;
            //this.ShowInTaskbar = true;
            MyNotifyIcon.Visible = true;
            ShowNotification("Protecting your data");

            MyNotifyIcon.MouseClick += MyNotifyIcon_MouseClick;

            // start watermark
            //ShowNotification("Watermark");
            //Process.Start(@"C:\Program Files\Rainmeter\Rainmeter.exe");
            //ShowNotification("start watermark");
            WaterMark wm = new WaterMark(33, 0.12, userID);
            wm.Show();
            
            // Heartbeat
            Trace.WriteLine("Heartbeat");
            heartbeatWorker.DoWork += heartbeatWorker_DoWork;
            heartbeatWorker.RunWorkerAsync();

            Trace.Write("Facial Recognition");
            facialWorker.DoWork += facialWorker_DoWork;
            facialWorker.RunWorkerAsync();
            //heartbeatWorker.RunWorkerCompleted += heartbeatWorker_RunWorkerCompleted;
        }

        // Override
        protected override void OnSourceInitialized(EventArgs e)
        {
            Trace.WriteLine("OnSourceInit");
            base.OnSourceInitialized(e);

            // Initialize the clipboard now that we have a window soruce to use
            var windowClipboardManager = new ClipboardManager(this);
            windowClipboardManager.ClipboardChanged += ClipboardChanged;
        }

        // CliboardChanged event
        private void ClipboardChanged(object sender, EventArgs e)
        {
            // Handle your clipboard update here, debug logging example:
            if (Clipboard.ContainsText())
            {
                //Clipboard.SetData(DataFormats.Text, (Object)copyPasteText);
                try
                {
                    Clipboard.Clear();
                    Clipboard.SetText(copyPasteText);
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(ex.ToString());
                }
            }
        }

        private void MyNotifyIcon_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            ShowNotification("clicked");
        }

        // Notification popup at bottom right corner
        public void ShowNotification(string msg)
        {
            MyNotifyIcon.BalloonTipTitle = "Safe Guard Chain";
            MyNotifyIcon.BalloonTipText = msg;
            MyNotifyIcon.ShowBalloonTip(3);
        }

        // Send heartbeat request periodically and check the response
        private void heartbeatRequest(string token, string sessionID)
        {
            Trace.WriteLine("Heartbeat");
            string url = globalApiEndpoint + "session/keepalive?sessionId=" + sessionID;
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", token);
                var request = new HttpRequestMessage(new HttpMethod("PATCH"), url);

                HttpResponseMessage response = null;
                try
                {
                    response = client.SendAsync(request).Result;
                    response.EnsureSuccessStatusCode();

                    Trace.WriteLine(response);
                    var jsonStrResponse = response.Content.ReadAsStringAsync().Result;
                    Trace.WriteLine(jsonStrResponse);
                }
                catch (HttpRequestException e)
                {
                    Trace.WriteLine(e.ToString());
                    ShowNotification("Session Disconnect. Force Logout by Admin.");
                    ShowMessageBox("Session Disconnect. Force Logout by Admin.                                                                                                               ");
                    Thread.Sleep(3000);
                    int result = ExitWindowsEx(
                        (uint)(ExitFlags.Force), (uint)(Reason.ApplicationIssue | Reason.PlannedShutdown));
                    if (result == 0) throw new Win32Exception(Marshal.GetLastWin32Error());
                }
            }
        }

        // Hearbeat worker to call hearbeatrequest continously
        private void heartbeatWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            while (hearbeatFlag)
            {
                heartbeatRequest(loginToken, loginSessionID);
                Thread.Sleep(5000);
            }

        }

        // Capture user face and store in a folder
        // Call face recognition function with the stored image and sessionID
        private void facialWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            capture = new VideoCapture(0);
            Mat frame = new Mat();
            capture.Open(0);
            Trace.WriteLine(captureInProgress);
            if (capture.IsOpened())
            {
                Trace.WriteLine("Is Open");
                while (captureInProgress)
                {
                    capture.Read(frame);
                    // The filename format cant use white space, :, -
                    string fileName = DateTime.Now.ToString("MMddyyyyHHmmss");
                    Trace.WriteLine(fileName);
                    string filePath = System.IO.Path.Combine(@"C:\SafeGuardChain\captures\", fileName + ".jpg");
                    frame.SaveImage(filePath);
                    Trace.WriteLine("Capturing image");
                    faceRecognition(loginToken, filePath, loginSessionID);
                    Thread.Sleep(10000);
                }
            }
        }

        // Customized message box visualization
        private void ShowMessageBox(string message)
        {
            var thread = new Thread(
                () =>
                {
                    MessageBox.Show(message, "Safe Guard Chain");
                });
            thread.Start();
        }

        // Stop user session and force logout
        private void stopSession(string token, string sessionID)
        {
            Trace.WriteLine("StopSession");
            string url = globalApiEndpoint + "session/stop?sessionId=" + sessionID;
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", token);
                var request = new HttpRequestMessage(new HttpMethod("PATCH"), url);

                HttpResponseMessage response = null;
                try
                {
                    response = client.SendAsync(request).Result;
                    response.EnsureSuccessStatusCode();

                    Trace.WriteLine(response);
                    var jsonStrResponse = response.Content.ReadAsStringAsync().Result;
                    Trace.WriteLine(jsonStrResponse);
                    int result = ExitWindowsEx(
                        (uint)(ExitFlags.Force), (uint)(Reason.ApplicationIssue | Reason.PlannedShutdown));
                    if (result == 0) throw new Win32Exception(Marshal.GetLastWin32Error());
                }
                catch (HttpRequestException e)
                {
                    Trace.WriteLine(e.ToString());
                }
            }
        }

        // Submit image and sessionID, return a score
        // Check if the score < 1, Yes -> Face recognition passed, No -> Failed
        // Passed: Continue
        // Failed: StopSession, ForceLogout
        private void faceRecognition(string token, string filePath, string sessionID)
        {
            Trace.WriteLine("Face Recogizing");
            string url = globalApiEndpoint + "facial/calculate";
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", token);

                // Creating Multipart form data
                using (var multiPartStream = new MultipartFormDataContent())
                {
                    string fileName = System.IO.Path.GetFileName(filePath);
                    Trace.WriteLine(fileName);
                    multiPartStream.Add(new StringContent(sessionID), "sessionId");

                    FileStream fs = File.OpenRead(filePath);
                    var streamContent = new StreamContent(fs);
                    var imageContent = new ByteArrayContent(streamContent.ReadAsByteArrayAsync().Result);
                    multiPartStream.Add(imageContent, "image", fileName);

                    HttpResponseMessage response = null;
                    try
                    {
                        response = client.PostAsync(url, multiPartStream).Result;
                        response.EnsureSuccessStatusCode();

                        Trace.WriteLine(response);
                        var jsonStrResponse = response.Content.ReadAsStringAsync().Result;
                        Trace.WriteLine(jsonStrResponse);
                        JObject json = JObject.Parse(jsonStrResponse);
                        var scoreStr = json["data"]["Score"];
                        double score = 1d;
                        double faceTheta = 1d;
                        try
                        {
                            score = Convert.ToDouble(scoreStr);
                            Trace.WriteLine(score);
                        }
                        catch (FormatException)
                        {
                            Console.WriteLine("Unable to convert '{0}' to a Double.", scoreStr);
                        }
                        catch (OverflowException)
                        {
                            Console.WriteLine("'{0}' is outside the range of a Double.", scoreStr);
                        }

                        // Check if the face recognition is passed
                        if (score < faceTheta)
                        {
                            Trace.WriteLine("Face recognition passed");
                        }
                        else
                        {
                            Trace.WriteLine("Face recognition failed");
                            Trace.WriteLine("AAA");
                            ShowNotification("Face not match. Force Logout!");
                            ShowMessageBox("Face not match. Force Logout!                                                                                                               ");
                            Thread.Sleep(30000);
                            stopSession(loginToken, loginSessionID);

                        }
                    }
                    catch (HttpRequestException e)
                    {
                        Trace.WriteLine(e.ToString());
                        Trace.WriteLine("Face recognition failed");
                        Trace.WriteLine("BBB");
                        ShowNotification("Face not match. Force Logout!");
                        ShowMessageBox("Face not match. Force Logout!                                                                                                               ");
                        Thread.Sleep(30000);
                        stopSession(loginToken, loginSessionID);
                    }
                }
            }
        }
    }
}
