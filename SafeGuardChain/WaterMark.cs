﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Runtime.InteropServices;
using System.Net.NetworkInformation;
using System.Drawing.Drawing2D;

namespace SafeGuardChain
{
    public partial class WaterMark : Form
    {
        private WaterMarkLabel watermarklabel;
        public WaterMark(float fontSize, double opacity, string userID)
        {
            InitializeComponent();
            SuspendLayout();
            if (opacity < 0.01 || opacity > 0.3)
            {
                opacity = 0.05;
            }
            if (fontSize < 15 || fontSize > 40)
            {
                fontSize = 20;
            }

            Opacity = opacity;
            AutoScaleDimensions = new SizeF(Screen.PrimaryScreen.WorkingArea.Width, Screen.PrimaryScreen.WorkingArea.Height);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = MaximumSize;
            ControlBox = false;
            FormBorderStyle = FormBorderStyle.None;
            MaximizeBox = false;
            MinimizeBox = false;
            ShowIcon = false;
            ShowInTaskbar = false;
            var hwnd = Handle;
            NativeMethods.SetWindowExTransparent(hwnd);
            TopMost = true;
            AllowTransparency = true;
            ResumeLayout(false);

            string macAddress = NetworkInterface.GetAllNetworkInterfaces().Where(nic => nic.OperationalStatus == OperationalStatus.Up && nic.NetworkInterfaceType != NetworkInterfaceType.Loopback).Select(nic => nic.GetPhysicalAddress().ToString()).FirstOrDefault();
            string content = "UserID: " + userID + " MacAddress: " + macAddress + " Time:" + getCurrentTime();

            watermarklabel = new WaterMarkLabel();
            watermarklabel.Anchor = (AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Bottom);
            watermarklabel.Font = new Font("Times New Roman Bold", fontSize);
            watermarklabel.ForeColor = SystemColors.ActiveCaptionText;
            //watermarklabel.Name = "WATERMARK";
            watermarklabel.TabIndex = 0;
            watermarklabel.NewText = content;
            watermarklabel.TextAlign = ContentAlignment.MiddleCenter;
            watermarklabel.RotateAngle = -45;
            

            Controls.Add(watermarklabel);
            WindowState = FormWindowState.Maximized;
        }

        private string getCurrentTime()
        {
            TimeZone timez = TimeZone.CurrentTimeZone;
            DateTime now = timez.ToUniversalTime(DateTime.Now);
            var hktz = TimeZoneInfo.FindSystemTimeZoneById("China Standard Time");
            var hkt = TimeZoneInfo.ConvertTimeFromUtc(now, hktz);
            string datetime = hkt.ToString("yyyy/MM/dd HH:mm:ss ");
            return datetime;
        }

        class WaterMarkLabel : Label
        {
            private Font finalFont = null;
            private int repeatCounter = 0;
            static int screenWidth = Screen.PrimaryScreen.WorkingArea.Width;
            static int screenHeight = Screen.PrimaryScreen.WorkingArea.Height;
            static int diagonal = (int)Math.Ceiling(Math.Sqrt(screenHeight * screenHeight + screenWidth * screenWidth));

            public int RotateAngle { get; set; }
            public string NewText { get; set; }

            private void DrawRotatedTextAt(Graphics gr, float angle, string txt, int x, int y, Font the_font, Brush the_brush, RectangleF rect)
            {
                // Save the graphics state.
                GraphicsState state = gr.Save();
                gr.ResetTransform();

                // Rotate.
                gr.RotateTransform(angle);

                // Translate to desired position. Be sure to append
                // the rotation so it occurs after the rotation.
                gr.TranslateTransform(x, y, MatrixOrder.Append);

                // Draw the text at the origin.
                gr.DrawString(txt, the_font, the_brush, rect);

                // Restore the graphics state.
                gr.Restore(state);
            }
            protected override void OnPaint(PaintEventArgs e)
            {
                string finalText = "";
                RectangleF rect = new RectangleF();
                rect.Size = new Size(2 * diagonal, 2 * diagonal);

                if (finalFont is null || repeatCounter == 0)
                {
                    finalFont = null;
                    repeatCounter = 0;
                    int textHeight = (int)e.Graphics.MeasureString(NewText, Font, diagonal, StringFormat.GenericTypographic).Height;
                    while (textHeight < 2 * diagonal)
                    {
                        try
                        {
                            finalText += NewText;
                            repeatCounter += 1;
                            textHeight = (int)e.Graphics.MeasureString(finalText, Font, diagonal, StringFormat.GenericTypographic).Height;
                        }
                        catch
                        {
                            Font = new Font(Font.FontFamily, 1.25f * Font.Size);
                            textHeight = 0;
                            finalText = "";
                        }
                    }
                    finalFont = Font;
                }
                else
                {
                    for (int i = 0; i < repeatCounter; i++)
                    {
                        finalText += NewText;
                    }
                }
                DrawRotatedTextAt(e.Graphics, RotateAngle, finalText, -diagonal / 2, diagonal / 2, finalFont, Brushes.Black, rect);
            }
        }
    }
}
