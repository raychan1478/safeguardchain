﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.IO;

namespace SafeGuardChain
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            string[] configLines = File.ReadAllLines(@"c:\SafeGuardChain\config.txt");
            string[] endpointConfigLines = File.ReadAllLines(@"c:\SafeGuardChain\endpointConfig.txt");
            
            // Get the parameters from config.txt
            MainWindow wnd = new MainWindow(configLines[0], configLines[1], configLines[2], endpointConfigLines[0]);
            //MainWindow wnd = new MainWindow("eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1ODY5MzUxMDksIlRva2VuVHlwZSI6ImxldmVsMSIsIkluZm8iOnsiSXNzdWVyIjoiQkJjaGFpbiIsIlVzZXJJZCI6MSwiVXNlcm5hbWUiOiJzdXBlckFkbWluIiwiUm9sZSI6ImFkbWluIiwiR3JvdXBzIjoiW10ifX0.pZpRun88QGloEs-VEn2zt6KuRqVanS-LkkO3SI_E0x2jACnyNpZ5c8PeA817QNQ4HcD66l62pEn9898ZEORVbJqyhK6siV2w2QQRK5oX7W3gkEKYXjayPP-N_8rPxcOH5-CCxHvhy2V2k1UfNEyG037x6wuCJzo_aJvKtG9XJy_nofvcKhoBT6stvyWdwgzn_s0I153kYrsTSPD94FWcf3EhsZudmxURuHGb9-0Cx9yypBBgSqF7qYOnxdnuXCXXaO7k6rYyqQKgC-0N1DHtY-URl5A0wu9DJTJb6cfPaEJiQaUUdMF4qZDgk5LauUpHTlZyo4R797BNolSnSsQgtg", "5", "1");
            // Make the mainwindow disappear
            wnd.Width = 0;
            wnd.Height = 0;
            wnd.WindowStyle = WindowStyle.None;
            wnd.ShowInTaskbar = false;
            wnd.ShowActivated = false;
            wnd.ResizeMode = ResizeMode.NoResize;
            wnd.Visibility = Visibility.Hidden;
            wnd.AllowsTransparency = true;
            //wnd.Topmost = false;
            // Get the parameters from wincp
            //MainWindow wnd = new MainWindow(e.Args[0], e.Args[1], e.Args[2]);
            wnd.Show();
        }
    }
}
